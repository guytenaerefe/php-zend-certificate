<!doctype html>
<html lang="en">

    <head>
        <title>Zend Certification - Francesco de Guytenaere</title>
    </head>
    
    <body>
    <?php
        // Set Error Reporting
        ini_set('error_reporting', E_ALL);
        error_reporting(-1);

        // Build a recursive directory list
        function dir_to_array($dir) 
        {
            $result = array();
            $dir_list = scandir($dir);
            foreach ($dir_list as $item) {
                if (false == in_array($item, ['.', '..'])) {
                    if (is_dir($dir . DIRECTORY_SEPARATOR . $item)) {
                        $result[$item] = dir_to_array($dir . DIRECTORY_SEPARATOR . $item);
                    } else {
                        $result[$item] = str_replace(__DIR__, '', $dir);
                    }
                }
            }
            return $result;
        }

        // Cleanup the input
        function clean_up($input) {
            $start = [
                '_',
                '.php',
            ];

            $replace = [
                ' ', 
                ' ',
            ];

            $output = str_replace($start, $replace, $input);
            return ucwords($output);
        }

        // Format a list to html
        function format_list($list) {
            $result .= '<ul>';
            foreach($list as $key => $value) {
                if (is_array($value) || $list instanceOf Traversable && count($value) > 1) {
                    $result .= '<li>' . clean_up($key) . ':'; 
                    $result .= format_list($value) . '</li>';
                } elseif (strpos($key, '.php') !== false ) {
                    $result .= '<li><a href="'. $value . DIRECTORY_SEPARATOR . $key . '">' . clean_up($key) . '</a></li>'; 
                }
            }
            $result .= '</ul>';
            return $result;
        }

        // Use the current directory
        $dir = dirname(__FILE__);
        $list = dir_to_array($dir);
        $formatted_list = format_list($list);

        // Output the list
        echo $formatted_list;
    ?> 
    <p>Script that generates this page (Although the indentation seems to be messed up, for the original, follow the link) :</p>
    <script src="https://bitbucket.org/guytenaerefe/php-zend-certificate/src/df881b1fdb6e460d4c756f9f1a4bb1692f332af9/public/index.php?embed=t"></script>
    </body>
</html>
