<!doctype html>
<html lang="en">

    <head>
	    <title>Floating Point Numbers</title>
	</head>

    <body>
        <h1>Floating Point Numbers</h1>
        <p>
           Floating Point Numbers are also known as decimals, doubles or real numbers. They can be written like: 
        </p>
        <ul>
            <li>$a = 1.234;</li> 
            <li>$b = 1.2e3; (1200, can also be written as: 1.2 * 10 ** 3, exponentiation got added as an arithmic operator in php 5.6)) </li> 
            <li>$c = 7E-10;</li>
        </ul>

        <h2>Comparing floats</h2> 
        <p>
            Comparing floating point numbers for equality can be problematic, due to the value they represent eternally. Extracted from 
            a comment on php.net: <br />

            "While the author probably knows what they are talking about, this loss of precision has nothing to do with decimal notation, it has to do with representation as a floating-point binary in a finite register, such as while 0.8 terminates in decimal, it is the repeating 0.110011001100... in binary, which is truncated.  0.1 and 0.7 are also non-terminating in binary, so they are also truncated, and the sum of these truncated numbers does not add up to the truncated binary representation of 0.8 (which is why (floor)(0.8*10) yields a different, more intuitive, result).  However, since 2 is a factor of 10, any number that terminates in binary also terminates in decimal."
        </p>
        <p>
           For these type of calculations there are arbritrary precision math (bcmath) and gmp functions. 
        </p>
        <h2>NaN</h2>
        <p>
            Sometimes numeric operations can result in a value represented by the constant NAN. It can not be compared to by any value, only the is_nan() function can check this. 
        </p>

<?php

// echo floor((0.1+0.7)*10); 

?>

    </body>
</html>
