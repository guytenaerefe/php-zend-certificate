<!doctype html>
<html lang="en">

    <head>
	    <title>Integers</title>
	</head>
    
    <body>
        <h1>Integers</h1>
        <p>
            An integer is any whole number from the mathematical "integer", any number than can be written without a fractional component. This includes their
            additive reverses (negative numbers). The only difference between the integer in maths, is that in PHP they are not infinite, if the number is beyond the bounds of the integer type, it will be interpreted as a float instead. Also, an operation which results in a number beyond the bounds of the integer type will return a float instead. This is called integer overflow. If the float is larger than the maximum float value (depends on the system), it will be cast to the PHP predefined constant INF. Integer size can be set with PHP_INT_SIZE, integer max with PHP_INT_MAX and since php 7 the PHP_INT_MIN can also be set. 
        </p>
        <p>
            Integers can be specified in 4 base notation systems. These are:
        </p>
        <ul>
            <li>decimal (base 10)</li>
            <li>hexadecimal (base 16): precede the number with 0x to use this notation</li>
            <li>octal (base 8): precede the number with 0 to use this notation</li>
            <li>binary (base 2): precede the number with 0b to use this notation </li>
        </ul>
        <p>
            If an invalid digit is given in an octal integer (for example 8 or 9), the rest of the number will be ignored. Since PHP 7 a parse error is emitted.
        </p>
        
        <h1>Converting Integers</h1>
        <p>
            If you explicitly want to convert an integer, you can cast it with (int) or (integer). The function intval() also works, usually it is not necessairy
            to convert, since a value will automatically be converted when needed by a function, control structure or operator and an integer argument is required.
        </p>
        <h2>Bool to Integer</h2>
        <p>
            FALSE yields 0<br />
            TRUE yields 1 
        </p>
        <h2>Floating Point Numbers</h2>
        <p>
            When a float is converted to an integer, the number will always be rounded toward zero. If the float exceeds the boundaries of integer, it will
            be undefined. Note that NO warning or notice will be triggered when this happens!<br />
            <strong>Warning:</strong> never convert an unknown fraction to integer, as this may yield unexpected results.
        </p>
        <h2>Strings</h2>
        <p>
            As in the manual: this section will be implented in the string type chapter.
        </p>
        <h2>Other types</h2>
        <p>
            Converting other types to integers are undefined. Observed behaviour is unreliable, since it may change without notice.
        </p>
    </body>
</html>
