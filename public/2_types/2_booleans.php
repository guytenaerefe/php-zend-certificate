<!doctype html>
<html lang="en">

    <head>
	    <title>Booleans</title>
	</head>

    <body>
        <h1>Booleans</h1>
        <p>
            Booleans supposedly is most simple type in PHP. A boolean can be either TRUE or FALSE. (Note: This is case-insensitive!). It is true that it is pretty simple,
            but in some cases there can be weird behaviour, as will be explained below. 
            Typically, the result of an operator (TODO: Link to chapter Operators) which returns a boolean and then gets used in a control structure (TODO: link to Control structure)
        </p>
        <p>
            A value can be cast to a boolean by using (bool) or (boolean), in most cases this won't be needed, as a value will be automatically converted
            to a boolean when a boolean is required by an operator, function or control structure. This why it's important to know which values are considered false and 
            which values are considered true. The following values are considered FALSE:
        </p>        
        <ul>
            <li>a boolean with the value: FALSE</li>
            <li>an integer with the value: 0 (zero)</li>
            <li>a float with the value: 0.0 (zero)</li>
            <li>an empty string OR a string with the value "0"</li>
            <li>an array with zero elements</li>
            <li>in PHP 4 only: an object with zero member variables</li>
            <li>the special type NULL (including unset variables)</li>
            <li>SimpleXML objects created from empty tags</li>
        </ul>
        <p>Every other value is considered to be TRUE (including any resource). Like this, -1 is also considered TRUE, as it is non-zero.</p>
        <p>
            In my opinion, it is very strange that only the string "0" is considered FALSE, but the string "FALSE" is not, neither is "0.0". This 
            seems to be a kind of inconsistency which should be taken into account.
        </p>
    </body>
</html>
