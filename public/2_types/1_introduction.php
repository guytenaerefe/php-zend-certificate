<!doctype html>
<html lang="en">

    <head>
	    <title>Introduction to Types</title>
	</head>

    <body>
        <h1>Introduction to Types</h1>
        <p>PHP supports 8 primitive types</p>
        <p>Scalar types:</p>
        <ul>
            <li>boolean</li>
            <li>integer</li>
            <li>float (floating-point number, also known as double)</li>
            <li>string</li>
        </ul>
        <p>Compound types:</p>
        <ul>
            <li>Array</li>
            <li>Object</li>
        </ul>
        <p>Special types:</p>
        <ul>
            <li>resource</li>
            <li>NULL</li>
        </ul>
        <p>Pseudo-types</p>
        <ul>
            <li>mixed: a parameter may accept multiple (but not necessarily all) types. (gettype() accepts all, str_replace() only string and array</li>
            <li>number: a parameter can be either a float or an integer</li>
            <li>Callback: Callable (NOTE TODO: EXPAND INFO ON CALLABLE)</li>
            <li>array|object: parameter can be either an array or an object</li>
            <li>void: as a return type means that the return value is useless, in a parameter list it means the function won't accept parameters </li>
            <li>$... : in a function prototype this means and so on. This means the function can take an endless number of arguments</li>
        </ul>
    </body>
</html>
