    <h1>PHP Basics</h1>

    <h2>Subjects</h2>
    <ul>
        <li>Syntax</li>
        <li>Operators</li>
        <li>Variables</li>
        <li>Control Structures</li>
        <li>Language Constructs & Functions</li>
        <li>Constants</li>
        <li>Namespaces</li>
        <li>Extensions</li>
        <li>Configuration</li>
        <li>Performance</li>
    </ul>

    <h2>Syntax</h2>

    <h3>Punctuation</h3>
    <ul>
        <li>Terminate code statements with a semicolon (;)</li>
        <li>Use appropriate tags</li>
    </ul>

    <h3>Tags</h3>
    <p>
        When PHP is parsing a file, it will look for opening and closing tags.
        The following tags are acceptable:
    </p>
    <ul>                     
        <li>Normal: <code>&lt;?php ... ?&gt;</code></li>
        <li>Short tag: <code>&lt;?</code> (Note: Discouraged. Only works when enabled in php.ini or php was configured with <code>--enable-short-tags</code></li>
        <li><code>&lt;= 'print this string' &gt;;</code> (Note: echo tag. Always available since 5.4 and up)</li>
        <li><code>&lt;script language="php"&gt; ... &lt;/script&gt;</code> (Note: Removed in PHP 7) </li>
        <li><code>&lt;% ... %&gt; and &lt;%= 'this is printed' %&gt;</code> (Note: Discouraged. Only works when enabled in php.ini. Removed in PHP 7</li>
    </ul>
    <p>In php "only" files, that contain only PHP, it is encouraged to omit the closing tag.</p>

    <h3>Comments</h3>
    <p>There are 3 ways to add comments to your PHP code:</p>
    <ul>
        <li><code>// this is a comment</code></li>
        <li>
            <code>
                /*  this is a comment block that can span<br />
                    multiple lines */
            </code>
        </li>
        <li><code># shell style comment</code></li>
    </ul>
    <p>Note: this can cause parse errors when using regular expressions. Example: /* $regx->setPattern('/^\d.*/'); */</p>

    <h3>Escaping HTML</h3>

    <p>Everything outside of PHP tags will be ignored by the PHP parser, that way files can have mixed content.</p>
    &lt;?php if ($expression == true): ?&gt;<br />
    this is true<br />
    &lt;?php else: ?&gt;<br />
    Otherwise this will show.<br />
    &lt;?php endif; ?&gt;<br />
    </p>
    <p>If you are embedding PHP into XML or XHTML, you need to use the "normal" PHP tags to be compliant with the standards.</p>

    <h2>Operators</h2>
    

