
        <h1>Introduction</h1>
        <p>
            This webpage, including subpages, will be structured according to the Zend PHP 5.5 Certification Study Guide. This introduction page
            will contain general information, pointers and explanations about terms used in the document. 
        </p>
        <p>
            <h2>The exam itself</h2>
        </p>
        <ul>
            <li>The exams consists of approximately 70 randomly generated questions</li>
            <li>Questions will vary in level of difficulty</li>
            <li>The exam will be based on PHP 5.5</li>
            <li>Questions will often test more than one concept at a time</li>
            <li>Questions will cover ten different topic areas (as listed below)</li>
            <li>You will have to finish the exam within 90 minutes</li>
            <li>You will be able to mark questions for review, so you can skip them and quickly return later</li>
            <li>There are three types of questions:
                <ul>
                    <li>Multiple Choice; 1 answer correct (Most frequent)</li>
                    <li>Multiple Choice; Two or more answers correct (Will be indicated)</li>
                    <li>Free Text (Open answer); Short Answer (No whitetext, comments, large code blocks), Example: identify a function, it's parameters or analyze code</li>
                </ul>
            </li>
            <!--li>... Perhaps more will follow!</li!-->
        </ul>
        <p>
            <h2>The ten topic areas of the questions</h2>
        </p>

        <!-- TODO: Make these bulletpoints hyperlinks to corresponding pages !-->
        <ul>
            <li><a href="?p=basics">PHP Basics</a></li>
            <li><a href="?p=data">Data Formats and Types</a></li>
            <li><a href="?p=strings">Strings</a></li>
            <li><a href="?p=arrays">Arrays</a></li>
            <li><a href="?p=io">Input / Output</a></li>
            <li><a href="?p=functions">Functions</a></li>
            <li><a href="?p=oop">Object Oriented Programming</a></li>
            <li><a href="?p=dbs">Databases</a></li>
            <li><a href="?p=security">Security</a></li>
            <li><a href="?p=webfeatures">Web Features</a></li>
        </ul>
            
        <h3>Some topics are given more weight in the certification</h3>
        <p>
            <strong>Highest emphasis:</strong>
        </p>
        <ul>
            <li>PHP Basics</li>
            <li>Security</li>
            <li>Object Oriented Programming (OOP)</li>
        </ul>

        <p>
            <strong>Average emphasis:</strong>
        </p>
        <ul>
            <li>Functions</li>
            <li>Web Features</li>
            <li>Arrays</li>
            <li>Strings and Patterns</li> 
        </ul>

        <p>
            <strong>Lowest emphasis:</strong>
        </p>
        <ul>
            <li>Databases</li>
            <li>Data Formats and Types</li>
            <li>Input / Output</li>
        </ul>
   
        <p>
            The guide stresses that being an expert in only some topics will NOT be enough. Knowledge should be derived from all topics. However,
            within a topic there are particular areas of concentration, these will be listed on the first page of each section in the Study Guide.
        </p>
